# Anime Game Protos
This repository contains the [ProtoBuf](https://github.com/google/protobuf) `.proto` files for Anime Game API.<br/>

## NOTE
You can use this directly with GC but there are some functions that don't work because they mix with proto 3.4/3.3, such as: gacha, mail, map point. the fix is still ongoing.

## Source 
 - [Crepe](https://git.crepe.moe/crepe-inc/crepe-protos) (3.5) <br/>
 - [CloudyPS](https://github.com/CloudyPS/protos/) (3.5) <br/>
 - [NickTheHuy (Hiro)](https://github.com/NickTheHuy/3.5_protos) (3.5) <br/>
 - [Sorapointa Team](https://github.com/Sorapointa/Sorapointa-Protos) (<3.3)